library(ggplot2)

## A pleasant Bayesian prior and posterior
##
## 1. Use =theme_bw= to get a very simple base.
## 2. Use a =theme= to remove all of additional panel lines.
## 3. Use =scale_*_continuous= to get nice breaks
## 4. Use =geom_segment= with =Inf= values for the axis lines.
##
## a. Use an area plot for the prior distribution.
## b. Use a line plot for the posterior.
##

x        <- seq(from = 0, to = 1, by = 0.01)
prior_df <- data.frame(x = x, y = dbeta(x, 3, 2))
post_df  <- data.frame(x = x, y = dbeta(x, 3, 5))

g <- ggplot() +
  geom_area(prior_df, mapping = aes(x = x, y = y), alpha = 0.1) +
  geom_line(post_df, mapping = aes(x = x, y = y)) +
  list(
    theme_bw(),
    theme(panel.border = element_blank(),
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank()),
    labs(x = "Parameter", y = "Density")
  ) +
  scale_x_continuous(breaks = seq(from = 0, to = 1, by = 1)) +
  geom_segment(aes(x = 0, xend = 1, y = -Inf, yend = -Inf)) +
  geom_segment(aes(x = -Inf, xend = -Inf, y = 0, yend = 3))
