

# Navigation alias
alias bibliography="cd ~/Documents/bibliography/"
alias cdc_remote="ssh -i ~/.ssh/cdc aez@130.56.249.92"
alias doc="cd ~/Documents/"
alias dot="cd ~/Documents/dotfiles/"
alias el="cd ~/Documents/el-99/"
alias gorilla="cd ~/Documents/gorilla/"
alias journal="cd ~/Documents/journal/"
alias bibliography="cd ~/Documents/bibliography/"
alias library="cd ~/Documents/library/"

# Git
# Add all modified tracked files.
alias gsd="git status ."
alias ga="git add"
alias gc="git commit -m"
alias gd="git diff"