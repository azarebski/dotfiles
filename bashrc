#!/usr/bin/env bash

# Run the following to cache the bitbucket password for an hour.
# $ git config --global credential.helper 'cache --timeout 3600'

# Emacs
org2html () {
    # Generate a HTML file from an orgmode file and open this in the default
    # browser.
    #
    # Example
    #   $ org2html demo.org
    #
    echo "Generating html..."
    emacs $1 --batch -f org-html-export-to-html --kill
    echo "Opening html..."
    sensible-browser $(basename $1 .org).html
}

# Browser
popera () {
    # Open a URL in a new private window in Opera.
    #
    # Example
    #   $ popera www.google.com
    echo "Opening in private window..."
    /Applications/Opera.app/Contents/MacOS/Opera --private $1
}


source $HOME/Documents/dotfiles/secret

#xmodmap -e 'keycode 66 = Escape'
#xmodmap -e 'clear Lock'
#xmodmap -e 'pointer = 3 2 1'



if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
