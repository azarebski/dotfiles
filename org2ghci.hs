import Data.List (intercalate)
import System.Environment (getArgs)
import System.Process (callCommand)

cmd as = "ghci " ++ as

srcStart = "#+BEGIN_SRC haskell"

srcEnd = "#+END_SRC"


extract :: [String] -> [String]
extract = \ls -> step ([], ls)

step :: ([String], [String]) -> [String]
step (hs, []) = hs
step (hs, os) = step $ addNewLine $ takeHaskell $ dropOrg (hs, os)
  where
    addNewLine (hs, os) = (hs ++ ["\n"], os)

dropOrg :: ([String], [String]) -> ([String], [String])
dropOrg (hs, []) = (hs, [])
dropOrg (hs, os) = (hs, temp2)
  where
    temp1 = dropWhile (/=srcStart) os
    temp2 = if null temp1 then [] else tail temp1

takeHaskell :: ([String], [String]) -> ([String], [String])
takeHaskell = shiftUntil (==srcEnd)

shiftUntil :: (String -> Bool) -> ([String], [String]) -> ([String], [String])
shiftUntil _ (lls, []) = (lls, [])
shiftUntil p (lls, rh:rls)
  | p rh      = (lls, rls)
  | otherwise = shiftUntil p (lls ++ [rh], rls)

main = do
  args <- getArgs
  a <- readFile (head args)
  writeFile tmpFile $ intercalate "\n" $ extract $ lines a
  callCommand $ cmd tmpFile
  where
    tmpFile = "sldkfjsdlfkj.hs"
